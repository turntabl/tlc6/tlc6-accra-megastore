package io.turntabl.accramegastore.services.product;

import io.turntabl.accramegastore.model.Product;
import io.turntabl.accramegastore.services.ProductService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ProductServiceTest {


    @Test
    public void loadProducts_shouldSucceed_givenValidCsv() {
        // given a valid list of products in a csv file.
        String filename = "products/valid_products.csv";
        List<Product> expected = List.of(
                new Product("001", "Frozen Green Peas", "Fresho" , 270.00, 10, "Frozen Vegetables"),
                new Product("002", "Tea - Natural Care", "Red Label", 650.00, 10, "Beverages")
        );

        // when I call the load products method
        List<Product> actual =  new ProductService(filename).getProducts();

        // then all the products should be successfully loaded.
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void loadProducts_shouldSkipProduct_ifPriceIsInvalid() {
        // given a list of valid and invalid products
        String filename = "products/products_with_invalid_selling_price.csv";
        List<Product> expected = List.of(new Product("001", "Frozen Green Peas", "Fresho" , 270.00, 10, "Frozen Vegetables"));

        // when I call the load products method
        List<Product> actual = new ProductService(filename).getProducts();

        // then only the valid products should be loaded
        assertThat(actual).isEqualTo(expected);
    }


    @Test
    public void loadProducts_shouldSkipProduct_ifQuantityIsInvalid() {

        // given a list of valid and invalid products
        String filename = "products/products_with_invalid_quantity.csv";
        List<Product> expected = List.of(new Product("001", "Frozen Green Peas", "Fresho" , 270.00, 10, "Frozen Vegetables"));

        // when I call the load products method
        List<Product> actual =  new ProductService(filename).getProducts();

        // then only the valid products should be loaded
        assertThat(actual).isEqualTo(expected);
    }

}