package io.turntabl.accramegastore.services.discount;

import io.turntabl.accramegastore.model.Item;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;

class MultibuyDiscountServiceTest {

    @Test
    public void shouldCalculateDiscount() {
        // given a list of products
        List<Item> expected = List.of(
                new Item("001", "Frozen Green Peas", "Fresho", 270.00, 3, "Groceries"),
                new Item("002", "Tea - Natural Care", "Red Label", 50.00, 6, "Beverages"),
                new Item("003", "Frozen Green Peas", "Fresho", 650.00, 2, "Groceries")
        );

        // when I call the discount method
        MultibuyDiscountService underTest = new MultibuyDiscountService();

        double discount = underTest.calculateDiscount(expected);

        // then I should return discount amount
        Assertions.assertThat(discount).isEqualTo(370);
    }

}