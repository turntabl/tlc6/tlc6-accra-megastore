package io.turntabl.accramegastore.services.discount;

import io.turntabl.accramegastore.model.Item;

import java.util.List;

public class HalfPriceDiscountService implements DiscountService {
    public double calculateDiscount(List<Item> items) {
        double sum = 0d;

        for (Item item : items) {
            sum += item.getQuantity() * item.getPrice();
        }

        return sum * 0.5;
    }
}
