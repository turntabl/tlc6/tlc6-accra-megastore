package io.turntabl.accramegastore.services.discount;

import io.turntabl.accramegastore.model.Item;

import java.util.List;

public interface DiscountService {

    double calculateDiscount(List<Item> items);
}
