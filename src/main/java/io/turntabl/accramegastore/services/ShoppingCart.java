package io.turntabl.accramegastore.services;

import io.turntabl.accramegastore.exception.UnavailableProductException;
import io.turntabl.accramegastore.model.Item;
import io.turntabl.accramegastore.model.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class ShoppingCart {

    private final Map<String, Item> cartItems;

    public ShoppingCart() {
        this.cartItems = new HashMap<>();
    }

    public void addProduct(Product product, int quantity) {
        if (quantity < 1) {
            throw new RuntimeException("quantity is not valid");
        }

        if (product.getStockLevel() < quantity) {
            String message = format("product with id=%s is unavailable in the requested quantity", product.getId());
            throw new UnavailableProductException(message);
        }

        String productId = product.getId();

        if (cartItems.containsKey(productId)) {
            Item item = cartItems.get(productId);
            item.setQuantity(item.getQuantity() + quantity);
        } else {
            cartItems.put(productId, new Item(productId, product.getName(),
                    product.getBrand(), product.getPrice(), quantity,
                    product.getCategory()));
        }
    }

    public void removeProduct(Product product, int quantity) {
        Item item = cartItems.get(product.getId());

        String productId = product.getId();

        if (!cartItems.containsKey(productId)) {
            throw new UnavailableProductException("Basket does not contain item: " + productId);
        }

        if (quantity > item.getQuantity() || quantity < 0) {
            throw new RuntimeException("Quantity to remove must be between: 1 and " + item.getQuantity());
        }

        int newQuantity = item.getQuantity() - quantity;

        if (newQuantity == 0) {
            cartItems.remove(productId);
        } else {
            item.setQuantity(newQuantity);
        }
    }

    public double calculateTotal() {
        return cartItems.values().stream()
                .map(item -> item.getPrice() * item.getQuantity())
                .reduce(0d, Double::sum);
    }

    public List<Item> getItems() {
        return cartItems.values().stream().toList();
    }
}
