package io.turntabl.accramegastore;

import io.turntabl.accramegastore.exception.UnavailableProductException;
import io.turntabl.accramegastore.exception.UnknownCommandException;
import io.turntabl.accramegastore.exception.UnknownProductException;
import io.turntabl.accramegastore.model.Item;
import io.turntabl.accramegastore.model.Product;
import io.turntabl.accramegastore.services.ProductService;
import io.turntabl.accramegastore.services.ShoppingCart;
import io.turntabl.accramegastore.services.discount.DiscountService;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Megastore {

    private final ShoppingCart shoppingCart;
    private final ProductService productService;
    private final DiscountService discountService;
    private final Scanner scanner;

    private static final String RETURN = "return";
    private static final String YES = "y";

    public Megastore(ProductService productService,
                     DiscountService discountService,
                     ShoppingCart shoppingCart,
                     Scanner scanner) {
        this.productService = productService;
        this.discountService = discountService;
        this.shoppingCart = shoppingCart;
        this.scanner = scanner;
    }

    public void run() {
        Command command = null;

        do {
            System.out.println("Enter one of the following commands: " + Arrays.toString(Command.values()));
            try {
                command = Command.parse(scanner.next());

                switch (command) {
                    case DISPLAY -> display();
                    case ADD -> add();
                    case REMOVE -> remove();
                    case CHECKOUT -> checkout();
                }
            } catch (UnknownCommandException e) {
                System.out.println(e.getMessage());
            }
        } while(command != Command.EXIT);
    }

    private void display() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("---------------\n");
        stringBuilder.append("Items in Store \n");
        stringBuilder.append("---------------\n");

        List<Product> products = productService.getProducts();
        for (Product product: products) {
            stringBuilder.append(product);
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder);
    }

    private void add() {
        System.out.println("Enter the product id to add " +
                "(or type `return` to go back to the main menu): ");

        String input = scanner.next();

        try {
            if (input.equals(RETURN)) return;
            String productId = input;
            Product product = productService.getProduct(productId);
            shoppingCart.addProduct(product, 1);
            productService.updateProduct(productId, product.getStockLevel() - 1);

            System.out.printf("Item: %s successfully added. Enter \"y\" to add another product or anythings else to return to the main menu. \n", input);
            input = scanner.next();

            if (input.equals(YES)) {
                add();
            }
        } catch (UnknownProductException | UnavailableProductException e) {
            System.out.print(e.getMessage() + ", ");
            add();
        }
    }

    private void remove() {
        System.out.println("Enter the product id to remove " +
                "(or type `return` to go back to the main menu): ");

        String input = scanner.next();

        try {
            if (input.equals(RETURN)) return;
            String productId = input;
            Product product = productService.getProduct(productId);
            shoppingCart.removeProduct(product, 1);
            productService.updateProduct(productId, product.getStockLevel() + 1);
            System.out.printf("Item: %s successfully removed.", input);

        } catch (UnavailableProductException e) {
            System.out.println("The product you've selected is not in your basket. ");
            remove();
        } catch (UnknownProductException e) {
            System.out.println("The product you selected does not exist in the megastore. please try again...");
            remove();
        }
    }
    
    private void checkout() {
        List<Item> items = shoppingCart.getItems();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("---------------\n");
        stringBuilder.append("Items in Basket\n");
        stringBuilder.append("---------------\n");

        for (Item item: items) {
            stringBuilder.append(item);
            stringBuilder.append("\n");
        }

        double total = shoppingCart.calculateTotal();
        double discount = discountService.calculateDiscount(items);

        // display cart summary
        stringBuilder.append("---------------\n");
        stringBuilder.append("Total: \t\t\t" + total + "\n");
        stringBuilder.append("Discount: \t\t" + discount + "\n");
        stringBuilder.append("Total to Pay: \t" + (total - discount) + "\n");
        System.out.println(stringBuilder);

        // confirm checkout
        System.out.println("Enter \"y\" to confirm if you'd like to checkout or anything else to return to the main menu.");

        String input = scanner.next();
        if (input.equals(YES)) {
            exit();
        }
    }

    public void exit() {
        System.out.println("Thanks for visiting the Accra Mega Store, Goodbye!");
        System.exit(0);
    }
}
