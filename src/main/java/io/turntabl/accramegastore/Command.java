package io.turntabl.accramegastore;

import io.turntabl.accramegastore.exception.UnknownCommandException;

import java.util.Arrays;

enum Command {
    ADD, REMOVE, DISPLAY, CHECKOUT, EXIT;

    public static Command parse(String value) {
        for (Command command : Command.values()) {
            if (command.toString().equalsIgnoreCase(value)) {
                return command;
            }
        }
        throw new UnknownCommandException(
                "Unknown command: " + value +
                        ". Valid values are: " + Arrays.toString(Command.values()));
    }
}
