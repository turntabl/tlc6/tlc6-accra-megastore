package io.turntabl.accramegastore;

import io.turntabl.accramegastore.services.ProductService;
import io.turntabl.accramegastore.services.ShoppingCart;
import io.turntabl.accramegastore.services.discount.MultibuyDiscountService;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("Welcome to the Accra Mega Store!");

        Megastore megastore = new Megastore(
                new ProductService("products.csv"),
                new MultibuyDiscountService(),
                new ShoppingCart(),
                new Scanner(System.in)
        );

        megastore.run();
    }

}
