##  💲💲💲 Welcome to the Accra Mega Store 💲💲💲

The **Accra Mega Store Ltd** is a command line application. The entry point of the application is the `Application.
java` class.

Topics:
- Day 1: Intro to Spring Framework and Spring Boot
- Day 2: Intro to JDBC
- Day 3: Intro do JPA
- Day 4: Intro to the Web
- Day 5: Intro to the Web prt 2 (e.g. Spring Security)